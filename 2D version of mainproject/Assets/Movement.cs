using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    public GameObject player;
    public float jumpForce;
    public Transform TF;
    private Rigidbody2D rb;
    private Vector3 mVector;
    public float speed;
    // Start is called before the first frame update
    void Start()
    {
        TF = gameObject.GetComponent<Transform>();
        rb = gameObject.GetComponent<Rigidbody2D>();
        mVector = new Vector3(speed, 0.0f, 0.0f); 
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.D))
        {
            TF.Translate(mVector*Time.deltaTime) ;
        }
        else if (Input.GetKey(KeyCode.A))

        {

            TF.Translate(-mVector * Time.deltaTime);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Jump();
        }
    }



     void Jump()
    {
        rb.velocity = Vector2.up * jumpForce;

    }
}
